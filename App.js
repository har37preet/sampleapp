import React, { Component } from "react";
import thunk from "redux-thunk";

import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import RootNavigator from "./common/navigators/root-navigator";
import allReducer from "./common/reducers";
import { NavigationContainer } from "@react-navigation/native";

import NotificationService from "./common/notifications/NotificationServices";
import SplashScreen from "react-native-splash-screen";
// create store and link it with developer tools
const store = createStore(
  allReducer,
  applyMiddleware(thunk),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

class App extends Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true;

  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    XMLHttpRequest = GLOBAL.originalXMLHttpRequest
      ? GLOBAL.originalXMLHttpRequest
      : GLOBAL.XMLHttpRequest;

    return (
      <View>
        <Text>Test App</Text>
      </View>
    );
  }
}

export default App;
