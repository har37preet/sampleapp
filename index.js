/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import messaging from '@react-native-firebase/messaging';
import NotificationService from './common/notifications/NotificationServices';

AppRegistry.registerComponent(appName, () => App);

messaging().setBackgroundMessageHandler((notification) => {
	const service = new NotificationService('');
	service.localNotification(notification);
});
